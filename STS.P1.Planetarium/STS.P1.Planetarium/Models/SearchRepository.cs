﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class SearchRepository : ISearchRepository
    {
        private PlanetariumDB _db = new PlanetariumDB();

        public IList<MovieModel> SearchMovies(string searchText)
        {
            var query = from m in _db.Movie
                        where m.MovieName.ToLower().Contains(searchText.ToLower())
                        select m;

            var items = query.ToList<MovieModel>();
            return items;
        }
        
    }
}