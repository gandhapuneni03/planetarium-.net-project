﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public interface IBookingRepository
    {
        IList<PlanetariumModel> GetAllPlanetariums();
        IList<MovieModel> GetAllMoviesByPlanetarium(int planetariumId);
        IList<MovieModel> GetAllMovieDetailsByMovieId(int movieId);
    }
}