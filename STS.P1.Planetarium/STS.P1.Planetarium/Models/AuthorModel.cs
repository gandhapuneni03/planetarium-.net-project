﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace STS.P1.Planetarium.Models
{
    public class AuthorModel
    {
        [Key]
        public int AuthorId { get; set; }
        public String AuthorName { get; set; }        
        //ForeignKey - MovieId
        public int MovieId { get; set; }
        public MovieModel MovieModel { get; set; }        
    }
}
