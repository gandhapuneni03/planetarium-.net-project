﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Models
{
    public class PlanetariumModel
    {        
     
        [Key] public int PlanetariumId { get; set; }        
        public String PlanetariumName { get; set; }
        public Boolean IsActive { get; set; }
        public virtual ICollection<MovieModel> MovieModels { get; set; }                           
             
    }
}