﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class PlanetModel
    {
        [Key]
        public int PlanetId { get; set; }
        public String PlanetName { get; set; }
        public String PlanetDescription { get; set; }
        public String PlanetVideoURL { get; set; }
        public Boolean IsActive { get; set; } 
    }
}