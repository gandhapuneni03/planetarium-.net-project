﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class BookingRepository : IBookingRepository
    {
        private PlanetariumDB _db = new PlanetariumDB();

        public IList<PlanetariumModel> GetAllPlanetariums()
        {
            var query = from r in _db.Planetarium
                        where r.IsActive == true
                        select r;

            var items = query.ToList<PlanetariumModel>();
            return items;
        }


        public IList<MovieModel> GetAllMoviesByPlanetarium(int planetariumId)
        {
            var query = from r in _db.Movie
                        where r.IsActive == true && r.PlanetariumId == planetariumId
                        select r;

            var items = query.ToList<MovieModel>();
            return items;
        }

        public IList<MovieModel> GetAllMovieDetailsByMovieId(int movieId)
        {
            var query = from r in _db.Movie
                        where r.IsActive == true && r.MovieId == movieId
                        select r;

            var item = query.ToList<MovieModel>();
            return item;
        }

    }
}