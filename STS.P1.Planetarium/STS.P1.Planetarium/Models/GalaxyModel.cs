﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class GalaxyModel
    {
        [Key]
        public int GalaxyId { get; set; }
        public String GalaxyType { get; set; }
        public String GalaxyName { get; set; }
        public String GalaxyDescription { get; set; }
        public String GalaxyVideoURL { get; set; }
        public Boolean IsActive { get; set; }        
    }
}