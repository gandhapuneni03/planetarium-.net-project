﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Models
{
    public class MovieModel
    {
        
        public MovieModel()
        {
                 
        }

        [Key] public int MovieId { get; set; }     
        [Required]
        public String MovieName { get; set; }
        
        public String Show1 { get; set; }
        public String Show2 { get; set; }
        public String Show3 { get; set; }
        public String Show4 { get; set; }
        [Required]
        public double TicketPrice { get; set; }
        public String TrailerURL { get; set; }
        public String ReleaseDate { get; set; }
        public double MovieLength { get; set; }
        public Boolean IsActive { get; set; }
        public virtual ICollection<BookingModel> BookingModels { get; set; }  

        //ForeignKey - PlanetariumId
        public int PlanetariumId { get; set; }
        public PlanetariumModel PlanetariumModel { get; set; }       

    }
}