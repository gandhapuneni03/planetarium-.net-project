﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class ConspiracyModel
    {
        [Key]
        public int ConspiracyId { get; set; }
        public String ConspiracyName { get; set; }        
        public String ConspiracyDescription { get; set; }
        public String ConspiracyCountry { get; set; }
        public String ConspiracyURL { get; set; }
        public String ConspiracyVideoURL { get; set; }
        public Boolean IsActive { get; set; } 
    }
}