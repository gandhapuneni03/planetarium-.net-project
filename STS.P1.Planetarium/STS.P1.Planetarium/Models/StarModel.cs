﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class StarModel
    {
        [Key]
        public int StarId { get; set; }
        public String StarName { get; set; }
        public String StarConstellation { get; set; }
        public String StarDescription { get; set; }
        public String StarVideoURL { get; set; }
        public Boolean IsActive { get; set; } 
    }
}