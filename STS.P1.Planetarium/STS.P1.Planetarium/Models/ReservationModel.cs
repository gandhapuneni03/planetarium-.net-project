﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Models
{
    public class ReservationModel
    {
        public ReservationModel()
        {
            AvailablePlanetariums = new List<SelectListItem>();
            AvailableMovieTimings = new List<SelectListItem>();  
            AvailableMovieDetails =  new MovieModel();
        }
        public IList<SelectListItem> AvailablePlanetariums { get; set; }
        public IList<SelectListItem> AvailableMovieTimings { get; set; }
        public MovieModel AvailableMovieDetails { get; set; }        
    }
}