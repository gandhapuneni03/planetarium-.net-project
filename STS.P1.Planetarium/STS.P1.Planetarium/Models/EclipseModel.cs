﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class EclipseModel
    {
        [Key]
        public int EclipseId { get; set; }
        public String EclipseType { get; set; }
        public String EclipseYear { get; set; }
        public String EclipseDate { get; set; }
        public String EclipseTime { get; set; }        
        public Boolean IsActive { get; set; } 
    }
}