﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Models
{
    public class DocumentModel
    {
        [Key] 
        public int DocumentId {get; set;}
        public String DocumentName {get; set;}
        public String DocumentDescription {get; set;}
        public String LocationURL {get; set;}
        public DateTime updloadDate {get; set;}
        public Boolean IsActive { get; set; }       

    }
}