﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class PlanetariumDB: DbContext
    {
        public PlanetariumDB()
            : base("name=STSPlanetarium")
        {

        }
                
        public DbSet<PlanetariumModel> Planetarium { get; set; }
        public DbSet<MovieModel> Movie {get; set;}        
        public DbSet<BookingModel> Booking { get; set; }
        public DbSet<AuthorModel> Author { get; set; }
        public DbSet<DocumentModel> Document { get; set; }        
        public DbSet<ConspiracyModel> Conspiracy { get; set; }
        public DbSet<EclipseModel> Eclipse { get; set; }
        public DbSet<GalaxyModel> Galaxy { get; set; }
        public DbSet<PlanetModel> Planet { get; set; }
        public DbSet<SliderModel> Slider { get; set; }
        public DbSet<StarModel> Star { get; set; } 

    }
}