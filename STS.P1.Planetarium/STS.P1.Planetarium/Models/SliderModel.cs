﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STS.P1.Planetarium.Models
{
    public class SliderModel
    {
        [Key]
        public int SliderId { get; set; }
        public String SliderTitle { get; set; }
        public String SliderDescription { get; set; }
        public String SliderButtonTitle { get; set; }
        public String SliderImageURL { get; set; }
        public Boolean IsActive { get; set; } 
    }
}