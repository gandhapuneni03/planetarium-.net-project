﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(STS.P1.Planetarium.Startup))]
namespace STS.P1.Planetarium
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
