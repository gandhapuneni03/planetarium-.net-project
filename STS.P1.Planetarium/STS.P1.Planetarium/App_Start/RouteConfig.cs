﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace STS.P1.Planetarium
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute("GetMoviesById",
                            "booking/getmoviesbyid/",
                            new { controller = "Booking", action = "GetMoviesById" },
                            new[] { "STS.P1.Planetarium.Controllers" });

            routes.MapRoute("Search",
                            "Search/Search/",
                            new { controller = "Search", action = "Search" },
                            new[] { "STS.P1.Planetarium.Controllers" });

            routes.MapRoute("GetMovieDetialsById",
                           "booking/getmoviedetialsbyid/",
                           new { controller = "Booking", action = "GetMovieDetialsById" },
                           new[] { "STS.P1.Planetarium.Controllers" });
        }
    }
}
