﻿using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
