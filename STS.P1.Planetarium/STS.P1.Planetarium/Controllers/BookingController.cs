﻿using STS.P1.Planetarium.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Controllers
{
    public class BookingController : Controller
    {
        private BookingRepository db = new BookingRepository();
        PlanetariumDB planetariumDatabase = new PlanetariumDB();
        private ReservationModel reservationModel = new ReservationModel();
        private MovieModel movieModel = new MovieModel();
        private float? reservationAmount;
        private String currentMovieId;

        
        // GET: Booking
        public ActionResult Reservation()
        {
            var planetariums = db.GetAllPlanetariums().ToList<PlanetariumModel>();

            reservationModel.AvailablePlanetariums.Add(new SelectListItem { Text = "Choose a Planetarium...", Value = "0" });
            foreach (PlanetariumModel planetarium in planetariums)
            {
                reservationModel.AvailablePlanetariums.Add(new SelectListItem { Text = planetarium.PlanetariumName, Value = planetarium.PlanetariumId.ToString() });                
            }

            ViewData["PlanetariumDropdown"] = reservationModel.AvailablePlanetariums;
                        
            return View();          
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetMoviesById(string planetariumId)
        {
            if (String.IsNullOrEmpty(planetariumId))
            {
                throw new ArgumentNullException("planetariumId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(planetariumId, out id);            
            var movies = db.GetAllMoviesByPlanetarium(id);
            var result = (from s in movies
                          select new
                          {
                              id = s.MovieId,
                              name = s.MovieName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetMovieDetialsById(string movieId)
        {
            if (String.IsNullOrEmpty(movieId))
            {
                throw new ArgumentNullException("movieId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(movieId, out id);            
            var MovieResult = db.GetAllMovieDetailsByMovieId(id);
            var result = (from r in MovieResult
                         select new
                         {
                             MovieId = r.MovieId,
                             MovieName = r.MovieName,
                             Show1 = r.Show1,
                             Show2 = r.Show2,
                             Show3 = r.Show3,
                             Show4 = r.Show4,
                             TicketPrice = r.TicketPrice,
                             TrailerURL = r.TrailerURL,
                             MovieLength = r.MovieLength,
                             IsActive = r.IsActive,
                             ReleaseDate = r.ReleaseDate
                         }).FirstOrDefault();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public RedirectToRouteResult GetMovieDetialsById(String ReservationTotalAmount, String MovieDropdown)
        {
            var amount = Convert.ToSingle(ReservationTotalAmount.Replace(" $",""));
            var MovieId = MovieDropdown;
            return RedirectToAction("Payment", new { ReservationTotalAmount = amount, MovieId = MovieDropdown });
        }


        public ActionResult Payment(float? ReservationTotalAmount, String MovieId)
        {
            reservationAmount = ReservationTotalAmount;
            currentMovieId = MovieId;
            return View();
        }

        [HttpPost]
        public ActionResult Payment(BookingModel book,String Name, String EmailId, String ContactNo, String StreeName, String CityName, String StateCode, String zipCode, String cardNumber, String cardExpiryDate, String cvvCode)
        {
            //reservationAmount = ReservationTotalAmount;
            String bookingId = "ABCDEF123";              
            BookingModel bookingModel = new BookingModel { Name = Name, EmailId = EmailId, ContactNo = ContactNo, BookingAmount = "25.00", StreeName = StreeName, CityName = CityName, StateCode = StateCode, zipCode = zipCode, cardNumber = cardNumber, cardExpiryDate = cardExpiryDate, cvvCode = cvvCode, MovieId = 2, IsActive = true, BookingToken = bookingId, NoOfPersons = "2" };
            if (bookingModel != null)
            {
                if (ModelState.IsValid)
                {
                    planetariumDatabase.Booking.Add(bookingModel);
                    planetariumDatabase.SaveChanges();
                }
                else
                {
                    return View(book);
                }
            }
            
            return RedirectToAction("PaymentConfirmation", new { BookingToken = bookingId });
        }

        public ActionResult PaymentConfirmation(String bookingToken)
        {

            ViewData["BookingToken"] = bookingToken;
            return View();
        }
    }
}