﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using STS.P1.Planetarium.Models;
using System.IO;

namespace STS.P1.Planetarium.Controllers
{
    public class TechnicalController : Controller
    {
        PlanetariumDB planetariumDatabase = new PlanetariumDB();
        BookingRepository planetariumDB = new BookingRepository();
        private ReservationModel reservationModel = new ReservationModel();
        private static readonly string _awsAccessKey = ConfigurationManager.AppSettings["_awsAccessKey"];

        private static readonly string _awsSecretKey = ConfigurationManager.AppSettings["_awsSecretKey"];

        private static readonly string _bucketName = ConfigurationManager.AppSettings["_awsBucketName"];
        private BookingRepository db = new BookingRepository();

        String returnResponse = "";
                
        // GET: Technical
        public ActionResult UploadDocument()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        // GET: Technical
        public ActionResult UploadDocument(string technicalDocument, string DocumentName, string business_url)
        {
            string fileName = "";

            foreach(String fileDocument in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[fileDocument] as HttpPostedFileBase;
                fileName = hpf.FileName;
                fileName = fileName.Substring(fileName.LastIndexOf("\\")+1);
                PutObjectResponse response;

                if (User.Identity.IsAuthenticated)
                {                   

                    try
                    {
                        IAmazonS3 client;
                        using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(_awsAccessKey, _awsSecretKey, Amazon.RegionEndpoint.USWest2))
                        {
                            var request = new PutObjectRequest()
                            {
                                BucketName = _bucketName,
                                CannedACL = S3CannedACL.PublicRead,//PERMISSION TO FILE PUBLIC ACCESIBLE
                                Key = string.Format("UPLOADS/{0}", fileName),
                                InputStream = hpf.InputStream//SEND THE FILE STREAM                                
                            };
                            
                            response = client.PutObject(request);
                                                                                    
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.resultDocumentMessage = "Document not submitted - " + ex.Message;
                        return View();
                    }

                    String documentURL = ConfigurationManager.AppSettings["_awsDocumentPath"] + string.Format("UPLOADS/{0}", fileName);
                    
                    ViewBag.resultDocumentMessage = "Document Uploaded Succesfully...";
                    DocumentModel docModel = new DocumentModel { DocumentName = DocumentName, LocationURL = documentURL, updloadDate = DateTime.Now, DocumentDescription = "This document is not a technical paper" };
                    if (docModel != null)
                    {
                        planetariumDatabase.Document.Add(docModel);
                        planetariumDatabase.SaveChanges();
                    }
                    return View();
                }
            }
            return RedirectToAction("Login", "Account");
        }

        public ActionResult EditUploadedDocument()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Login", "Account");
        }

        public ActionResult publish()
        {
            if (User.Identity.IsAuthenticated)
            {
                var planetariums = planetariumDB.GetAllPlanetariums().ToList<PlanetariumModel>();
                reservationModel.AvailablePlanetariums.Add(new SelectListItem { Text = "Choose a Planetarium...", Value = "0" });
                foreach (PlanetariumModel planetarium in planetariums)
                {
                    reservationModel.AvailablePlanetariums.Add(new SelectListItem { Text = planetarium.PlanetariumName, Value = planetarium.PlanetariumId.ToString() });
                }

                ViewData["PlanetariumDropdown"] = reservationModel.AvailablePlanetariums;                

                return View();
            }
            return RedirectToAction("Login", "Account");
            
        }

        [HttpPost]
        public ActionResult publish(MovieModel reservationObj)
        {

            //var planetariums = db.GetAllPlanetariums().ToList<PlanetariumModel>();
            PlanetariumDB planetDB = new PlanetariumDB();

            
            if (User.Identity.IsAuthenticated)
            {                
                MovieModel model = new MovieModel();
                model.MovieName = reservationObj.MovieName;
                model.Show1 = reservationObj.Show1;
                model.Show2 = reservationObj.Show2;
                model.Show3 = reservationObj.Show3;
                model.Show4 = reservationObj.Show4;
                model.TicketPrice = reservationObj.TicketPrice;
                model.TrailerURL = reservationObj.TrailerURL;
                model.ReleaseDate = reservationObj.ReleaseDate;
                model.MovieLength = reservationObj.MovieLength;
                model.IsActive = reservationObj.IsActive;
                model.PlanetariumId = 1;

                planetDB.Movie.Add(model);
                planetDB.SaveChanges();
                ViewBag.resultPublishMessage = "Movie published Sucessfully...!";
                return RedirectToAction("publish");
            }
            return RedirectToAction("Login", "Account");

        }


        public ActionResult publishEdit()
        {
            if (User.Identity.IsAuthenticated && Session["AccountKind"].Equals("A"))
            {
                return View();
            }
            return RedirectToAction("Login", "Account");
        }



    }
}