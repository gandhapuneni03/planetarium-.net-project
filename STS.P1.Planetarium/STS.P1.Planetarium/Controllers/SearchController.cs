﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using STS.P1.Planetarium.Models;

namespace STS.P1.Planetarium.Controllers
{
    public class SearchController : Controller
    {
        private SearchRepository db = new SearchRepository();


        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(string searchText, string searchOption)
        {
            var movies = db.SearchMovies(searchText);
            var result = (from s in movies
                          select new
                          {
                              movieId = s.MovieId,
                              movieName = s.MovieName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}