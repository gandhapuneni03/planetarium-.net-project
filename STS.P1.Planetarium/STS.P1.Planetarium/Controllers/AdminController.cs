﻿using STS.P1.Planetarium.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STS.P1.Planetarium.Controllers
{
    public class AdminController : Controller
    {
        ApplicationDbContext appDB = new ApplicationDbContext();

        // GET: Admin
        public ActionResult Index()        
        {            
            IEnumerable<STS.P1.Planetarium.Models.ApplicationUser> resultObj = from s in appDB.Users
                                                                              select s;
            return View(resultObj);
        }

        public ActionResult Edit(String id)
        {
            ApplicationDbContext loginModel = new ApplicationDbContext();
            ApplicationUser applicationUser = (ApplicationUser)(from s in loginModel.Users
                             where s.Email == id
                             select s).FirstOrDefault();
             return View(applicationUser);
        }

        public ActionResult Details(String id)
        {
            ApplicationDbContext loginModel = new ApplicationDbContext();
            ApplicationUser applicationUser = (ApplicationUser)(from s in loginModel.Users
                                                                where s.Email == id
                                                                select s).FirstOrDefault();
            return View(applicationUser);
        }

        public ActionResult Delete(String id)
        {
            ApplicationDbContext loginModel = new ApplicationDbContext();
            ApplicationUser applicationUser = (ApplicationUser)(from s in loginModel.Users
                                                                where s.Email == id
                                                                select s).FirstOrDefault();

            return View(applicationUser);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(String id)
        {
            ApplicationDbContext loginModel = new ApplicationDbContext();
            ApplicationUser applicationUser = (ApplicationUser)(from s in loginModel.Users
                                                                where s.Email == id
                                                                select s).FirstOrDefault();

            loginModel.Users.Remove(applicationUser);
            loginModel.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}